import React from "react";
import { Route, Routes, Navigate } from "react-router-dom";
import FixedPlugin from "components/FixedPlugin/FixedPlugin.js";
import routes from "routes.js";

import { BackgroundColorContext } from "contexts/BackgroundColorContext";

function Regis(props) {
  const getRoutes = (routes) => {
    return routes.map((prop, key) => {
      if (prop.layout === "/regis") {
        return (
          <Route path={prop.path} element={prop.component} key={key} exact />
        );
      } else {
        return null;
      }
    });
  };

  return (
    <BackgroundColorContext.Consumer>
      {({ color, changeColor }) => (
        <React.Fragment>
          <div className="wrapper">
            <div className="main-panel" data={color}>
              <Routes>
                {getRoutes(routes)}
                <Route
                  path="/"
                  element={<Navigate to="/regis/regis" replace />}
                />
              </Routes>
            </div>
          </div>
          <FixedPlugin bgColor={color} handleBgClick={changeColor} />
        </React.Fragment>
      )}
    </BackgroundColorContext.Consumer>
  );
}

export default Regis;