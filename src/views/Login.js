import React from "react";
import {
  Button,
  ButtonGroup,
  Card,
  CardFooter,
  CardHeader,
  CardBody,
  CardColumns,
  CardTitle,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  UncontrolledDropdown,
  Label,
  Form,
  FormGroup,
  Input,
  Table,
  Row,
  Col,
  UncontrolledTooltip,
} from "reactstrap";
import Reactstrap from "reactstrap";
import LoginCss from "./Login.css";
import { withRouter } from '../contexts/RouterContext';

// import { browserHistory } from 'react-router'

const FLEX_CENTER = {
  display: 'flex',
  flexDirection: 'row',
  justifyContent: 'center',
  alignItems: 'center'
};

const NO_PADDING = {
  padding: '0'
};

class Login extends React.Component {
  constructor(props) {
    super();
    this.state = {
      username: '',
      password: ''
    };
  }

  onBtnClick = (e) => {
    console.log('this.props: ', this.props);
    console.log('clicked');
    console.log('username: ', this.state.username);
    console.log('password: ', this.state.password);
    this.props.navigate('/admin/dashboard');
  }

  render = () => {
    return (
      <>
        <div className="content" style={{ ...FLEX_CENTER, ...NO_PADDING }}>
          <Row style={{ ...FLEX_CENTER, ...NO_PADDING, width: '25%' }}>
            <Col>
              <Card className="justify-content-center align-items-center">
                <CardHeader>
                  <h5 className="title">Input your credentials</h5>
                </CardHeader>
                <CardBody style={{ width: '100%' }}>
                  <Form>
                    <Row>
                      <Col>
                        <FormGroup name="loginForm" id="loginForm">
                          <label>Username</label>
                          <Input
                            placeholder="Username"
                            type="text"
                            onChange={(e) => this.setState({ username: e.target.value })}
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col>
                        <FormGroup name="loginForm" id="loginForm">
                          <label>Password</label>
                          <Input
                            placeholder="Password"
                            type="password"
                            onChange={(e) => this.setState({ password: e.target.value })}
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                  </Form>
                </CardBody>
                <CardFooter>
                  <Button className="btn-fill" color="primary" type="submit" onClick={this.onBtnClick}>
                    Login
                  </Button>
                </CardFooter>
              </Card>
            </Col>
          </Row>
        </div>
      </>
    );
  }
}

export default withRouter(Login);