import React from "react";
import {
  Button,
  ButtonGroup,
  Card,
  CardFooter,
  CardHeader,
  CardBody,
  CardColumns,
  CardTitle,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  UncontrolledDropdown,
  Label,
  Form,
  FormGroup,
  Input,
  Table,
  Row,
  Col,
  UncontrolledTooltip,
} from "reactstrap";
import Reactstrap from "reactstrap";
import LoginCss from "./Login.css";
import { withRouter } from '../contexts/RouterContext';

// import { browserHistory } from 'react-router'

const FLEX_CENTER = {
  display: 'flex',
  flexDirection: 'row',
  justifyContent: 'center',
  alignItems: 'center'
};

const NO_PADDING = {
  padding: '0'
};

class Regis extends React.Component {
  constructor(props) {
    super();
    this.state = {
      username: '',
      email: '',
      numphone: '',
      city: '',
      password: '',
      cpassword: ''
    };
  }

  onBtnClick = (e) => {
    console.log('this.props: ', this.props);
    console.log('clicked');
    console.log('username: ', this.state.username);
    console.log('email: ', this.state.email);
    console.log('numphone: ', this.state.numphone);
    console.log('city: ', this.state.city);
    console.log('password: ', this.state.password);
    console.log('cpassword: ', this.state.cpassword);
    this.props.navigate('/admin/dashboard');
  }

  render = () => {
    return (
      <>
        <div className="content" style={{ ...FLEX_CENTER, ...NO_PADDING }}>
          <Row style={{ ...FLEX_CENTER, ...NO_PADDING, width: '25%' }}>
            <Col>
              <Card className="justify-content-center align-items-center">
                <CardHeader>
                  <h5 className="title">Register</h5>
                </CardHeader>
                <CardBody style={{ width: '100%' }}>
                  <Form>
                    <Row>
                      <Col>
                        <FormGroup name="regisForm" id="regisForm">
                          <label>Username</label>
                          <Input
                            placeholder="Username"
                            type="text"
                            onChange={(e) => this.setState({ username: e.target.value })}
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col>
                        <FormGroup name="regisForm" id="regisForm">
                          <label>Email</label>
                          <Input
                            placeholder="email"
                            type="email"
                            onChange={(e) => this.setState({ email: e.target.value })}
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col>
                        <FormGroup name="regisForm" id="regisForm">
                          <label>Number Phone</label>
                          <Input
                            placeholder="numphone"
                            type="number"
                            onChange={(e) => this.setState({ numphone: e.target.value })}
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col>
                        <FormGroup name="regisForm" id="regisForm">
                          <label>City</label>
                          <Input
                            placeholder="city"
                            type="text"
                            onChange={(e) => this.setState({ city: e.target.value })}
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col>
                        <FormGroup name="regisForm" id="regisForm">
                          <label>Password</label>
                          <Input
                            placeholder="Password"
                            type="password"
                            onChange={(e) => this.setState({ password: e.target.value })}
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col>
                        <FormGroup name="regisForm" id="regisForm">
                          <label>Confirm Password</label>
                          <Input
                            placeholder="cpassword"
                            type="password"
                            onChange={(e) => this.setState({ cpassword: e.target.value })}
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                  </Form>
                </CardBody>
                <CardFooter>
                  <Button className="btn-fill" color="primary" type="submit" onClick={this.onBtnClick}>
                    Regist!
                  </Button>
                </CardFooter>
              </Card>
            </Col>
          </Row>
        </div>
      </>
    );
  }
}

export default withRouter(Regis);